﻿using System;
using System.Collections.Generic;
using System.Linq;
using Container;
using ScriptableObjects;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameLogic : MonoBehaviour
{
    public Variable<TeamMate> currentTeammate;
    public Variable<Question> currentQuestion;
    public Variable<float> currentProjectState;
    public Feuil1 questionsList;
    public Variable<int> dayCounter;
    
    public Button choice1;
    public Button choice2;
    public Button repondre;

    public TeamMate GD;
    public TeamMate SON;
    public TeamMate GRAPH;
    public TeamMate ERGO;
    public TeamMate Prog;
    public TeamMate Natkin;
    public TeamMate Aida;
    public TeamMate Jacky;
    public TeamMate Sabine;

    public GameObject answerPanel;
    public GameObject callPanel;
    public GameObject endGame;



    public EndSentences messagesDeFin;
    public TextMeshProUGUI gdEndMessage;
    public TextMeshProUGUI graphEndMessage;
    public TextMeshProUGUI progEndMessage;
    public TextMeshProUGUI ergoEndMessage;
    public TextMeshProUGUI sonEndMessage;
    public TextMeshProUGUI projectEndMessage;
    private List<MessageEnd> _messages = new List<MessageEnd>();
    
    
    private List<Question> _questions = new List<Question>();
    private List<TeamMate> listTeam = new List<TeamMate>();

    public MusicEmitter endGameMusicEmitter;
    
    private void Start()
    {
        listTeam = new List<TeamMate> { GD, Prog, GRAPH, ERGO, SON };  
        
        answerPanel.SetActive(true);
        callPanel.SetActive(false);
        endGame.SetActive(false);
        
        choice1.onClick.AddListener(OnChoice1);
        choice2.onClick.AddListener(OnChoice2);
        repondre.onClick.AddListener(OnAnswer);

        // Generate questions
        foreach (var quest in questionsList.dataArray)
        {
            if (quest.ROLE.Length == 0)
                continue;

            var i = new Question
            {
                question = quest.QUESTION, choice1 = (quest.CHOIX1, quest.POID1),
                choice2 = (quest.CHOIX2, quest.POID2)
            };
            
            switch (quest.ROLE)
            {
                case "GD":
                    i.type = GD;
                    break;
                case "Son":
                    i.type = SON;
                    break;
                case "Prog":
                    i.type = Prog;
                    break;
                case "Graph":
                    i.type = GRAPH;
                    break;
                case "Ergo":
                    i.type = ERGO;
                    break;
                case "Natkin":
                    i.type = Natkin;
                    break;
                case "Sabine":
                    i.type = Sabine;
                    break;
                case "Jacky":
                    i.type = Jacky;
                    break;
                case "Aida":
                    i.type = Aida;
                    break;
                default:
                    Debug.LogError("UNEXPECTED ROLE = " + quest.ROLE + " - " + quest.QUESTION);
                    break;
            }
            _questions.Add(i);
        }
        foreach (var msg in messagesDeFin.dataArray)
        {
            if(msg.Role.Length == 0)
                continue;
            
            MessageEnd m = new MessageEnd{sentence = msg.Phrase, sanity = msg.Etat};
            switch (msg.Role)
            {
                case "GD":
                    m.mate = GD;
                    break;
                case "Son":
                    m.mate = SON;
                    break;
                case "Prog":
                    m.mate = Prog;
                    break;
                case "Graph":
                    m.mate = GRAPH;
                    break;
                case "Ergo":
                    m.mate = ERGO;
                    break;
                case "Projet":
                    m.isProject = true;
                    break;
            }
            _messages.Add(m);
        }

        _questions.Shuffle();
        SetCurrentRandomQuestion();
    }

    private void OnAnswer()
    {
        answerPanel.SetActive(false);
        callPanel.SetActive(true);
    }

    void SetCurrentRandomQuestion()
    {
        Question t = GetRandom();
        currentQuestion.SetValue(t);
        currentTeammate.SetValue(t.type);
    }
    
    
    private Question GetRandom()
    {
        bool sameGuy = true;
        Question q = null;
        int r = 0;
        while (sameGuy)
        {
            r = Random.Range(0, _questions.Count);
            q = _questions[r];
            if (q.type != currentTeammate.Value)
                sameGuy = false;
        }
        _questions.RemoveAt(r);
        return q;
    }

    private Question RandomQ(List<Question> l)
    {
        if(l.Count == 0)
            return new Question
            {
                question = "ERREUR", choice1 = ("CODE AVEC LE CUL", 0),
                choice2 = ("LE CUL J'AI DIS", 0)
            };
        var r = Random.Range(0, l.Count);
        return l[r];
    }
    
    
    private void OnChoice1()
    {
        if(!currentTeammate.Value.isSpecial)
            currentTeammate.Value.IncreaseState();
        currentProjectState.Value += currentQuestion.Value.choice1.Item2/100f;
       
        EndOfTurn();
    }
    private void OnChoice2()
    {
        
        currentTeammate.Value.DecreaseState();
        currentProjectState.Value += currentQuestion.Value.choice2.Item2/100f;
        EndOfTurn();
    }



    TextMeshProUGUI MessageEnd(TeamMate mate)
    {
        return mate == GD ? gdEndMessage :
            mate == ERGO ? ergoEndMessage :
            mate == SON ? sonEndMessage :
            mate == GRAPH ? graphEndMessage :
            mate == Prog ? progEndMessage : projectEndMessage;
    }
    
    void PopulateEndScene()
    {
        foreach (TeamMate teamMate in listTeam)
        {
            int i = 0;
            
            if (teamMate.appearanceTime != 0)
            {
                if (teamMate.crunchTime / (float) teamMate.appearanceTime <= 0.33f)
                {
                    i = 0;
                }
                else if (teamMate.crunchTime / (float) teamMate.appearanceTime <= 0.66f)
                {
                    i = 1;
                }
                if (teamMate.crunchTime / (float) teamMate.appearanceTime >= 0.66f)
                {
                    i = 2;
                }
            }

            var message = _messages.FirstOrDefault((b => b.mate == teamMate && b.sanity == i));
            if (message == null)
                return;
            MessageEnd(teamMate).text = message.sentence;
        }
        
        if (dayCounter.Value <= 95)
            projectEndMessage.text = _messages.First(b => b.isProject && b.sanity == 0).sentence;
        else if (dayCounter.Value < 100)
            projectEndMessage.text = _messages.First(b => b.isProject && b.sanity == 1).sentence;
        else
            projectEndMessage.text = _messages.First(b => b.isProject && b.sanity == 2).sentence;
    }

    private void EndOfTurn()
    {
        callPanel.SetActive(false);
        if(currentProjectState.Value < 1f && dayCounter.Value < 100)
        {
            SetCurrentRandomQuestion();
            dayCounter.Value += 4;
            answerPanel.SetActive(true);
        }
        else
        {
            endGameMusicEmitter.PlayEvent();
            PopulateEndScene();
            endGame.SetActive(true);
        }
    }

}