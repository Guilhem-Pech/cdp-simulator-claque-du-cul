﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicStopper : MonoBehaviour
{
    public GameObject objectToDestroy;
    
    private void Start()
    {
        Destroy(objectToDestroy);
    }
}
