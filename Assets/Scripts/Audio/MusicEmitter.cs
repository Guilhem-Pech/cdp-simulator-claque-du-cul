﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicEmitter : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string audioButtonEvent;
    private FMOD.Studio.EventInstance _buttonEvent;

    public int destroySceneIndex;

    public bool playOnStart;

    void OnEnable()
    {
        SceneManager.sceneLoaded += LoadedScene;
    }
 
    void OnDisable()
    {
        SceneManager.sceneLoaded -= LoadedScene;
    }
    
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        _buttonEvent = FMODUnity.RuntimeManager.CreateInstance(audioButtonEvent);
    }

    private void Start()
    {
        if (playOnStart)
            _buttonEvent.start();
    }

    public void PlayEvent()
    {
        _buttonEvent.start();
    }

    private void LoadedScene(Scene scene,LoadSceneMode mode)
    {
        if (scene.buildIndex == destroySceneIndex)
        {
            _buttonEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            Destroy(gameObject);
        }
    }
}
