﻿using System;
using System.Collections;
using System.Collections.Generic;
using Container;
using FMOD.Studio;
using ScriptableObjects;
using UnityEngine;
using UnityEngine.Serialization;

public class AudioCallingBackground : MonoBehaviour
{
    [FormerlySerializedAs("callAudioEvent")] [FMODUnity.EventRef]
    public string voiceAudioEvent;
    private FMOD.Studio.EventInstance _voiceEvent;
    
    public Variable<TeamMate> currentMate;
    private EventInstance henri;

    private void Awake()
    {
        _voiceEvent = FMODUnity.RuntimeManager.CreateInstance(voiceAudioEvent);
    }

    private void OnEnable()
    {
         henri = currentMate.Value.GetFmodEvent(); //.start();
         henri.start();
         _voiceEvent.start();
    }
    
    private void OnDisable()
    {
        henri.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        _voiceEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }
}
