﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonAudio : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string audioButtonEvent;
    private FMOD.Studio.EventInstance _buttonEvent;

    private void Awake()
    {
        _buttonEvent = FMODUnity.RuntimeManager.CreateInstance(audioButtonEvent);
    }

    public void PlaySound()
    {
        _buttonEvent.start();
    }
}
