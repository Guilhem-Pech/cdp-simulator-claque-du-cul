﻿using System;
using System.Collections;
using System.Collections.Generic;
using Container;
using FMOD.Studio;
using ScriptableObjects;
using UnityEngine;

public class AudioCall : MonoBehaviour
{
    public Variable<TeamMate> currentTeamMate;

    private EventInstance henri4;

    private void OnEnable()
    {
        henri4 = currentTeamMate.Value.GetSonnerie();
        henri4.start();
    }

    private void OnDisable()
    {
        henri4.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }
}
