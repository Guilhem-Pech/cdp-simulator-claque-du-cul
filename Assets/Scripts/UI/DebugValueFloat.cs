﻿using System;
using ScriptableObjects;
using TMPro;
using UnityEngine;

namespace UI
{
    public class DebugValueFloat : MonoBehaviour
    {
        public Variable<float> variable;
        public string before;
        public string after;
        public TextMeshProUGUI textGO;

        private void Start()
        {
            textGO.text = $"{before}{variable.Value}{after}";
            variable.linkedEvent.AddListener(OnChangeValue);
        }

        private void OnChangeValue(float obj)
        {
            textGO.text = $"{before}{obj}{after}";
        }

        private void OnValidate()
        {
            if (textGO == null)
                textGO = GetComponent<TextMeshProUGUI>();
        }
    }
}