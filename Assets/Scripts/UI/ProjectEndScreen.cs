﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class ProjectEndScreen : MonoBehaviour
    {
        public string credit;
    
        public void Replay()
        {
            SceneManager.LoadScene(credit, LoadSceneMode.Single);
        }
    }
}