﻿using System;
using ScriptableObjects;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class TeamMate : MonoBehaviour
    {
        public Variable<Container.TeamMate> curTeamMate;
        public new TextMeshProUGUI name;
        public Image photo;
        
        
        private void Start()
        {
            name.text = curTeamMate.Value.name;
            photo.sprite = curTeamMate.Value.Img;
            curTeamMate.linkedEvent.AddListener(OnChangeTeammate);
            
        }

        private void OnChangeTeammate(Container.TeamMate obj)
        {
            name.text = obj.name;
            photo.sprite = obj.Img;
        }
    }
}