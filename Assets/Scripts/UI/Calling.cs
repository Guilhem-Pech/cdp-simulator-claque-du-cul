﻿using System;
using ScriptableObjects;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class Calling : MonoBehaviour
    {
        public Image avatarCall;
        public Variable<Container.TeamMate> peopleCalling;

        private void OnEnable()
        {
            avatarCall.sprite = peopleCalling.Value.avatar;
            peopleCalling.linkedEvent.AddListener(OnChangeTeamMate);
        }

        private void OnChangeTeamMate(Container.TeamMate obj)
        {
            avatarCall.sprite = obj.avatar;
        }

        private void OnDisable()
        {
            peopleCalling.linkedEvent.RemoveListener(OnChangeTeamMate);
        }
    }
}