﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Apple.ReplayKit;
using UnityEngine.SceneManagement;

public class CreditScreen : MonoBehaviour
{
    public string replayScene;
    
    public void Replay()
    {
        SceneManager.LoadScene(replayScene, LoadSceneMode.Single);
    }
}
