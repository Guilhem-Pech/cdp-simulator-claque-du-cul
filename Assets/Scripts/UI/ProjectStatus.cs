﻿using System;
using ScriptableObjects;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ProjectStatus : MonoBehaviour
    {
        public Image fillProgressBar;
        public Variable<float> projectState;
        private void Start()
        {
            projectState.linkedEvent.AddListener(OnChangeValue);
            fillProgressBar.fillAmount = projectState.Value;
        }

        private void OnChangeValue(float val)
        {
            fillProgressBar.fillAmount = val;
        }
    }
}