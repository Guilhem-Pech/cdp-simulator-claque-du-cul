﻿using System;
using ScriptableObjects;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class DayCounter : MonoBehaviour
    {
        public Variable<int> dayCounter;
        public TextMeshProUGUI text;
        public Image progressBar;
        public int maxDay = 100;

        private void Start()
        {
            text.text = $"Jour {dayCounter.Value} / {maxDay}";
            progressBar.fillAmount = dayCounter.Value / (float) maxDay;
            dayCounter.linkedEvent.AddListener(OnChangeDay);
        }

        private void OnChangeDay(int obj)
        {
            text.text = $"Jour {obj} / {maxDay}";
            progressBar.fillAmount = obj / (float) maxDay;
        }
    }
}