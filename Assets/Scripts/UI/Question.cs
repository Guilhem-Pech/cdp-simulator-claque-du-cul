﻿using System;
using ScriptableObjects.Variables;
using TMPro;
using UnityEngine;

namespace UI
{
    public class Question : MonoBehaviour
    {
        public QuestionVariable questionVariable;
        public TextMeshProUGUI question;
        public TextMeshProUGUI choiceOne;
        public TextMeshProUGUI choiceTwo;
        
        private void OnEnable()
        {
            var t = questionVariable.Value;
            question.text = t.question;
            choiceOne.text = t.choice1.Item1;
            choiceTwo.text = t.choice2.Item1;
            
            questionVariable.linkedEvent.AddListener(OnChangeQuestion);
        }

        private void OnDisable()
        {
            questionVariable.linkedEvent.RemoveListener(OnChangeQuestion);
        }

        private void OnChangeQuestion(Container.Question t)
        {
            question.text = t.question;
            choiceOne.text = t.choice1.Item1;
            choiceTwo.text = t.choice2.Item1;
        }
    }
}