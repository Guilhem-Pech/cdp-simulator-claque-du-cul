﻿namespace Container
{
    [System.Serializable]
    public class Question
    {
        public string question;
        public (string,float) choice1;
        public (string,float) choice2;
        public TeamMate type;
    }
}