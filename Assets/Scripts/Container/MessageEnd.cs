﻿using System;

namespace Container
{
    [Serializable]
    public class MessageEnd
    {
        public int sanity;
        public bool isProject = false;
        public TeamMate mate;
        public string sentence;
    }
}