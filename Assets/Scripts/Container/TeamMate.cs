﻿using System;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

namespace Container
{
    [Serializable]
    [CreateAssetMenu(menuName = "ScriptableObjects/TeamMate")]
    public class TeamMate : ScriptableObject
    {
        private void OnEnable()
        {
            sanity = 0;
            crunchTime = 0;
            appearanceTime = 0;
        }

        public bool isSpecial;
        public string name;
        public Sprite normal;
        public Sprite tired;
        public Sprite exhausted;
        public int sanity;
        public Sprite avatar;
        public Sprite Img => sanity == 0 ? normal : sanity == 1 ? tired : sanity == 2 ? exhausted : normal;

        public int crunchTime = 0;
        public int appearanceTime = 0;
        
        [FMODUnity.EventRef]
        public string fmodNormal;
        FMOD.Studio.EventInstance _eventNormal;
        
        [FMODUnity.EventRef]
        public string fmodTired;
        FMOD.Studio.EventInstance _eventTired;
        
        [FMODUnity.EventRef]
        public string fmodExhausted;
        FMOD.Studio.EventInstance _eventExhausted;

        
        [FMODUnity.EventRef]
        public string fmodSonnerie;
        FMOD.Studio.EventInstance _eventSonnerie;

        public FMOD.Studio.EventInstance GetSonnerie()
        {
            if (!_eventSonnerie.isValid())
                _eventSonnerie = FMODUnity.RuntimeManager.CreateInstance(fmodSonnerie);
            return _eventSonnerie;
        }
        
        public FMOD.Studio.EventInstance GetNormal()
        {
            if (!_eventNormal.isValid())
                _eventNormal = FMODUnity.RuntimeManager.CreateInstance(fmodNormal);
            return _eventNormal;
        }
        
        public FMOD.Studio.EventInstance GetTired()
        {
            if (!_eventTired.isValid())
                _eventTired = FMODUnity.RuntimeManager.CreateInstance(fmodTired);
            return _eventTired;
        }
        public FMOD.Studio.EventInstance GetExhausted()
        {
            if (!_eventExhausted.isValid())
                _eventExhausted = FMODUnity.RuntimeManager.CreateInstance(fmodExhausted);
            return _eventExhausted;
        }

        public FMOD.Studio.EventInstance GetFmodEvent()
        { 
            return sanity == 0 ? GetNormal() : sanity == 1 ? GetTired() : sanity == 2 ? GetExhausted() : GetNormal();   
        }
        
        public void IncreaseState()
        {    
            ++appearanceTime;
            ++crunchTime;
            sanity = math.clamp(++sanity,0,2);
        }
        
        public void DecreaseState()
        {
            ++appearanceTime;
            sanity = math.clamp(--sanity,0,2);
        }
    }
}