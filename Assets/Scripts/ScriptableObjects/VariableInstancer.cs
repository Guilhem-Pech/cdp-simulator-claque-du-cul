﻿using System;
using UnityEngine;

namespace ScriptableObjects
{
    /// <summary>
    /// A Variable Instancer is a MonoBehaviour that takes a variable as a base and creates an in memory copy of it OnEnable.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class VariableInstancer<T> : MonoBehaviour
    {
        /// <summary>
        /// The base variable the instancer copy from
        /// </summary>
        [SerializeField] private Variable<T> _base = null;
        private Variable<T> _inMemoryCopy;
        
        /// <summary>
        /// This pair event is raised when the variable is modified (pair event is the variable and the gameobject attached to)
        /// </summary>
        public PairEventVariable<T, GameObject> linkedPairEvent;
        
        /// <summary>
        /// This event is raised when the variable is modified
        /// </summary>
        public EventVariable<T> linkedEvent;
        
        /// <summary>
        /// Getter to the in-memory variable
        /// </summary>
        public Variable<T> Variable
        {
            get => _inMemoryCopy;
        }
        
        /// <summary>
        /// Getter and Setter to the in memory value
        /// </summary>
        public T Value
        {
            get => _inMemoryCopy.Value;
            set => _inMemoryCopy.Value = value;
        }

        private void OnEnable()
        {
            if (_base == null) return;
            _inMemoryCopy = Instantiate(_base);
            _inMemoryCopy.listenActions.Add(OnValueChange);
            _inMemoryCopy.linkedEvent = linkedEvent;
        }
        private void OnValueChange(T value)
        {
            linkedPairEvent.Raise(value,gameObject);
        }
    }
}