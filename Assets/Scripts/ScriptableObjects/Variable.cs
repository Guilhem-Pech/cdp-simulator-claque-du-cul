﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableObjects
{
    [Serializable]
    public class Variable<T> : ScriptableObject
    {
        private T _initialValue;
        private T _value = default;
        public EventVariable<T> linkedEvent;
        public List<Action<T>> listenActions = new List<Action<T>>();
        public T Value
        {
            get => _value;
            set => SetValue(value);
        }

        public T InitialValue;

        public void SetValue(T value)
        {
            if(linkedEvent != null)
                linkedEvent.Raise(value);
            foreach (var action in listenActions) action.Invoke(value);
            _value = value;
        }

        private void OnEnable()
        {
            _value = InitialValue; 
        }
    }
}
