﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ScriptableObjects
{
    [Serializable]
    public class EventVariable<T> : ScriptableObject
    {
        private List<Action<T>> _actions = new List<Action<T>>();
        private List<EventListener<T>> _eventListeners = new List<EventListener<T>>();
        
        public void AddListener(Action<T> action)
        {
            if (!_actions.Contains(action))
                _actions.Add(action);
        }
        public void RemoveListener(Action<T> action)
        {
            if (_actions.Contains(action))
                _actions.Remove(action);
        }

        public void AddListener(EventListener<T> action)
        {
            if(!_eventListeners.Contains(action))
             _eventListeners.Add(action);
        }
        
        public void RemoveListener(EventListener<T> action)
        {
            if(!_eventListeners.Contains(action))
                _eventListeners.Remove(action);
        }
        
        public void Raise(T value)
        {
            for (int i = _actions.Count - 1; i >= 0; --i)
            {
                var action = _actions[i];
                action.Invoke(value);
            }

            for (int i = _eventListeners.Count - 1; i >= 0; --i)
            {
                var action = _eventListeners[i];
                action.Invoke(value);
            }
        }
    }
}