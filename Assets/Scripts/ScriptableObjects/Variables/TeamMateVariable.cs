﻿using Container;
using UnityEngine;

namespace ScriptableObjects.Variables
{
    [CreateAssetMenu(menuName = "ScriptableObjects/Variable/TeamMateVariable")]
    public class TeamMateVariable : Variable<TeamMate>
    {
        
    }
}