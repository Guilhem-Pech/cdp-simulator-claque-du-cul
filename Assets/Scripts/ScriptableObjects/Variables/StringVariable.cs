﻿using UnityEngine;

namespace ScriptableObjects.Variables
{
    [CreateAssetMenu(menuName = "ScriptableObjects/Variable/StringVariable")]
    public class StringVariable : Variable<string>
    {}
    
}