﻿using UnityEngine;

namespace ScriptableObjects.Variables
{
    [CreateAssetMenu(menuName = "ScriptableObjects/Variable/FloatVariable")]
    public class FloatVariable : Variable<float>
    {}
}