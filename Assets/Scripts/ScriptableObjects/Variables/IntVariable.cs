﻿using UnityEngine;

namespace ScriptableObjects.Variables
{
    [CreateAssetMenu(menuName = "ScriptableObjects/Variable/IntVariable")]
    public class IntVariable : Variable<int>
    {}
}