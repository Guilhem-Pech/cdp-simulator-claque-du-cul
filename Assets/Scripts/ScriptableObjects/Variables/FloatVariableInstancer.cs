﻿using UnityEngine;

namespace ScriptableObjects.Variables
{
    public class FloatVariableInstancer : VariableInstancer<float>
    {}
}