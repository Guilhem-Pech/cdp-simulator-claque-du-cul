﻿using Container;
using UnityEngine;

namespace ScriptableObjects.Variables
{
    
    [CreateAssetMenu(menuName = "ScriptableObjects/Variable/QuestionVariable")]
    public class QuestionVariable : Variable<Question>
    {
        
    }
}