﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace ScriptableObjects
{
    public class EventListener<T> : MonoBehaviour
    {
        [SerializeField] private EventVariable<T> eventVariable;
        [SerializeField] private UnityEvent<T> response;
        
        public void Invoke(T value)
        {
            response.Invoke(value);
        }
        private void OnEnable()
        {
            eventVariable.AddListener(this);
        }

        private void OnDisable()
        {
            eventVariable.RemoveListener(this);
        }
    }
}