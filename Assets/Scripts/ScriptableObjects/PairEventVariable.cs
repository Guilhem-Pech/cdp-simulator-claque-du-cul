﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableObjects
{
    [Serializable]
    public class PairEventVariable<T,U> : ScriptableObject
    {
        private List<Action<T,U>> _actions = new List<Action<T,U>>();
        private List<PairEventListener<T,U>> _eventListeners = new List<PairEventListener<T,U>>();
        
        public void AddListener(Action<T,U> action)
        {
            if (!_actions.Contains(action))
                _actions.Add(action);
        }
        public void RemoveListener(Action<T,U> action)
        {
            if (_actions.Contains(action))
                _actions.Remove(action);
        }

        public void AddListener(PairEventListener<T,U> action)
        {
            if(!_eventListeners.Contains(action))
                _eventListeners.Add(action);
        }
        
        public void RemoveListener(PairEventListener<T,U> action)
        {
            if(!_eventListeners.Contains(action))
                _eventListeners.Remove(action);
        }
        
        public void Raise(T a, U b)
        {
            for (int i = _actions.Count - 1; i >= 0; --i)
            {
                var action = _actions[i];
                action.Invoke(a,b);
            }

            for (int i = _eventListeners.Count - 1; i >= 0; --i)
            {
                var action = _eventListeners[i];
                action.Invoke(a,b);
            }
        }
    }
}