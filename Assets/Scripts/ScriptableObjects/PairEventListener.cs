﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableObjects
{
    public class PairEventListener<T,U> : MonoBehaviour
    {
        [SerializeField] private PairEventVariable<T,U> eventVariable;
        [SerializeField] private UnityEvent<T,U> response;
        
        public void Invoke(T a, U b)
        {
            response.Invoke(a,b);
        }
        private void OnEnable()
        {
            eventVariable.AddListener(this);
        }

        private void OnDisable()
        {
            eventVariable.RemoveListener(this);
        }
    }
}