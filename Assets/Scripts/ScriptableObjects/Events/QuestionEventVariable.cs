﻿using Container;
using UnityEngine;

namespace ScriptableObjects.Events
{
    [CreateAssetMenu(menuName = "ScriptableObjects/Event/QuestionEventVariable")]
    public class QuestionEventVariable : EventVariable<Question>
    {}
}