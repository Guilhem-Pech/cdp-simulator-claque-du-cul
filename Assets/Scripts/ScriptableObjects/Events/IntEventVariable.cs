﻿using UnityEngine;

namespace ScriptableObjects.Events
{
    [CreateAssetMenu(menuName = "ScriptableObjects/Event/IntEventVariable")]
    public class IntEventVariable : EventVariable<int>
    {
        
    }
}