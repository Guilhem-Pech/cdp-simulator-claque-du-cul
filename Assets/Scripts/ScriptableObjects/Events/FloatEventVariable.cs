﻿using UnityEngine;

namespace ScriptableObjects.Events
{
    [CreateAssetMenu(menuName = "ScriptableObjects/Event/FloatEventVariable")]
    public class FloatEventVariable : EventVariable<float>
    {
        
    }
}