﻿using Container;
using UnityEngine;

namespace ScriptableObjects.Events
{
    [CreateAssetMenu(menuName = "ScriptableObjects/Event/TeamMateEventVariable")]
    public class TeamMateEventVariable : EventVariable<TeamMate>
    {
        
    }
}