﻿using UnityEngine;

namespace ScriptableObjects.Events
{
    [CreateAssetMenu(menuName = "ScriptableObjects/Event/StringEventVariable")]
    public class StringEventVariable : EventVariable<string>
    {}
}