using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
/// 
[System.Serializable]
public class Feuil1Data
{
  [SerializeField]
  string role;
  public string ROLE { get {return role; } set { this.role = value;} }
  
  [SerializeField]
  string question;
  public string QUESTION { get {return question; } set { this.question = value;} }
  
  [SerializeField]
  string choix1;
  public string CHOIX1 { get {return choix1; } set { this.choix1 = value;} }
  
  [SerializeField]
  string choix2;
  public string CHOIX2 { get {return choix2; } set { this.choix2 = value;} }
  
  [SerializeField]
  int poid1;
  public int POID1 { get {return poid1; } set { this.poid1 = value;} }
  
  [SerializeField]
  int poid2;
  public int POID2 { get {return poid2; } set { this.poid2 = value;} }
  
}