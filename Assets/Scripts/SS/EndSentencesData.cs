using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
/// 
[System.Serializable]
public class EndSentencesData
{
  [SerializeField]
  string role;
  public string Role { get {return role; } set { this.role = value;} }
  
  [SerializeField]
  int etat;
  public int Etat { get {return etat; } set { this.etat = value;} }
  
  [SerializeField]
  string phrase;
  public string Phrase { get {return phrase; } set { this.phrase = value;} }
  
}